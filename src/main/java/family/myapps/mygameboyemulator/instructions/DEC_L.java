package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Decrements the contents of register L.
 *
 * Opcode: 0x2D
 */
public class DEC_L extends DEC_R {

    @Override
    protected int getRegister() {
        return Cpu.State.L;
    }
}
