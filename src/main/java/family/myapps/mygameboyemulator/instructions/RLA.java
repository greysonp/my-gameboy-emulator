package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

import static family.myapps.mygameboyemulator.Cpu.State.A;

/**
 * Rotates the value of register A to the left.
 * The leftmost bit is placed in the carry flag, the previous value of the carry flag is placed in
 * bit 0 of register A.
 *
 * Opcode: 0x17
 */
public class RLA implements Instruction {

    @Override
    public void execute(Cpu.State cpu, Memory memory) {
        // Get the leftmost bit of A before we shift, so we can put it where it needs to go
        byte leftmostBit = (byte) (cpu.register[A] >> 7);

        // Shift the register
        cpu.register[A] = (byte) (cpu.register[A] << 1);

        // Put the value of the carry flag in bit 0 of A
        cpu.register[A] |= Flags.getC(cpu.flag);

        // The flags should all be cleared, except we set the carry flag equal to the leftmost bit
        cpu.flag = Flags.setC((byte) 0, leftmostBit);
    }

    @Override
    public int getCost() {
        return 4;
    }
}
