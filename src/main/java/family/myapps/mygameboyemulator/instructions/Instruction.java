package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

public interface Instruction {
    void execute(Cpu.State cpu, Memory memory);
    int getCost();
}
