package family.myapps.mygameboyemulator;

class Emulator {

    private final Cpu cpu;
    private final Memory memory;

    public Emulator(Cpu cpu, Memory memory) {
        this.cpu = cpu;
        this.memory = memory;
    }
}
