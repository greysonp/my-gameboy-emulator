package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

/**
 * Increments the contents of memory specified by the stack pointer by 1.
 *
 * Opcode: 0x33
 */
public class INC_SP implements Instruction {

    @Override
    public void execute(Cpu.State cpu, Memory memory) {
        byte currentValue = memory.readByte(cpu.sp);
        byte newValue = (byte) (currentValue + 1);
        memory.writeByte(cpu.sp, newValue);
    }

    @Override
    public int getCost() {
        return 8;
    }
}
