package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Increments the contents of register A by 1.
 *
 * Opcode: 0x3C
 */
public class INC_A extends INC_R {

    @Override
    protected int getRegister() {
        return Cpu.State.A;
    }
}
