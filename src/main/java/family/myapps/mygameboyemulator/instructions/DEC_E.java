package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Decrements the contents of register E.
 *
 * Opcode: 0x1D
 */
public class DEC_E extends DEC_R {

    @Override
    protected int getRegister() {
        return Cpu.State.E;
    }
}
