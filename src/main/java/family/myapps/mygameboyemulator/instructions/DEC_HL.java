package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Decrements the contents of memory specified by register pair HL 1.
 *
 * Opcode: 0x2B
 */
public class DEC_HL extends DEC_RR {

    @Override
    protected int getHighByteRegister() {
        return Cpu.State.H;
    }

    @Override
    protected int getLowByteRegister() {
        return Cpu.State.L;
    }
}
