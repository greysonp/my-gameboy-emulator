package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Bits;
import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

/**
 * Write the contents of the stack pointer to a specified location in memory.
 *
 * The first byte read is the low byte of the address, and the second byte read in the high byte
 * of the address.
 *
 * The low byte of the stack pointer is written to the address, and the high byte of the stack
 * pointer is written to next location after the address.
 *
 * Note that bytes are written low-then-high due to the CPU being little endian.
 *
 * Opcode: 0x08
 */
public class LD_NN_SP implements Instruction {

    @Override
    public void execute(Cpu.State cpu, Memory memory) {
        // Read the low and high bytes (remember, little endian)
        byte lowByte = cpu.program[cpu.pc];
        cpu.pc++;
        byte highByte = cpu.program[cpu.pc];
        cpu.pc++;

        // Form the memory address
        short address = Bits.joinBytes(highByte, lowByte);

        // Write the stack pointer to the address, in low-then-high byte order
        byte spHigh = Bits.getHighByte(cpu.sp);
        byte spLow = Bits.getLowByte(cpu.sp);

        memory.writeByte(address, spLow);
        memory.writeByte((short) (address + 1), spHigh);

    }

    @Override
    public int getCost() {
        return 20;
    }
}
