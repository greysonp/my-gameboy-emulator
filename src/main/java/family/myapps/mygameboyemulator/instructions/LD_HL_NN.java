package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Places a 16-bit word into the register pair HL.
 * H holds high byte, and L holds the low byte.
 *
 * Opcode: 0x21
 */
public class LD_HL_NN extends LD_RR_NN {

    @Override
    protected int getHighByteRegister() {
        return Cpu.State.H;
    }

    @Override
    protected int getLowByteRegister() {
        return Cpu.State.L;
    }
}
