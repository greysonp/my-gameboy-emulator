package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Bits;
import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

import static family.myapps.mygameboyemulator.Cpu.State.H;
import static family.myapps.mygameboyemulator.Cpu.State.L;

/**
 * Writes the contents of register A to the location in memory specified by the register pair HL,
 * then immediately increments the contents of HL by 1.
 *
 * Opcode: 0x22
 */
public class LD_HLI_A extends LD_RR_A {

    @Override
    public void execute(Cpu.State cpu, Memory memory) {
        super.execute(cpu, memory);

        short hl = Bits.joinBytes(cpu.register[H], cpu.register[L]);
        hl++;
        cpu.register[H] = Bits.getHighByte(hl);
        cpu.register[L] = Bits.getLowByte(hl);
    }

    @Override
    protected int getHighByteRegister() {
        return Cpu.State.H;
    }

    @Override
    protected int getLowByteRegister() {
        return Cpu.State.L;
    }
}
