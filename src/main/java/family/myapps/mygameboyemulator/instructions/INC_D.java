package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Increments the contents of register D by 1.
 *
 * Opcode: 0x14
 */
public class INC_D extends INC_R {

    @Override
    protected int getRegister() {
        return Cpu.State.D;
    }
}
