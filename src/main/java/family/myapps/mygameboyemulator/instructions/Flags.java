package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Bits;

/**
 * A helper class to determine what the flags are after a specific operation.
 */
public final class Flags {

    /**
     * Set if the last operation resulted in a value of zero.
     */
    final static byte Z = (byte) 0x80; // 1000 0000
    public final static byte Z_CLEAR = ~Z; // 0111 1111
    public static final int Z_POSITION = 7;

    /**
     * Set if the last operation was a subtraction.
     */
    final static byte N = (byte) 0x40; // 0100 0000
    public final static byte N_CLEAR = ~N; // 1011 1111
    public static final int N_POSITION = 6;

    /**
     * Set if the last operation resulted in a carry from bit 3 to 4.
     * e.g. (0000 1111) + (0000 0001) = (0001 0000)
     */
    final static byte H = (byte) 0x20; // 0010 0000
    public final static byte H_CLEAR = ~H; // 1101 1111
    public static final int H_POSITION = 5;

    /**
     * Set if the last operation resulted in overflowing the most significant bit.
     * e.g. (1111 1111) + (0000 0001) = (0000 0000)
     */
    final static byte C = (byte) 0x10; // 0001 0000
    public final static byte C_CLEAR = ~C; // 1110 1111
    public static final int C_POSITION = 4;


    /**
     * @param flag The original value of the F register
     * @param b The value of the register you are incrementing
     * @return The new value of the F register
     */
    public static byte incFlags(byte flag, byte b) {
        return addFlags(flag, b, (byte) 1, false);
    }

    public static byte addFlags(byte flag, byte b, byte addend) {
        return addFlags(flag, b, addend, true);
    }

    /**
     * @param flag The original value of the F register
     * @param b The value of the register you are adding to
     * @param addend The value you are adding to the register
     * @param adjustCarry Whether or not this should influence the carry flag
     * @return The new value of the F register
     */
    public static byte addFlags(byte flag, byte b, byte addend, boolean adjustCarry) {
        byte sum = (byte) (b + addend);

        // If the sum is zero, then set the Z flag, otherwise clear it
        if (sum == 0) {
            flag |= Z;
        } else {
            flag &= Z_CLEAR;
        }

        // This is not a subtraction, so we want to clear the N flag
        flag &= N_CLEAR;

        // If the sum's right nibble is less than the original right nibble, than we must have
        // overflowed the 3rd bit into the 4th bit, so we can set the H flag. Note that because we
        // are only checking the right nibble, we don't have to worry about the sign bit, and
        // therefore do not have to convert to an unsigned byte.
        byte originalRightNibble = (byte) (b & 0x0F);
        byte newRightNibble = (byte) (sum & 0x0F);
        if (newRightNibble < originalRightNibble) {
            flag |= H;
        } else {
            flag &= H_CLEAR;
        }

        if (adjustCarry) {
            // If the sum is less than the original value, we must have overflowed the last bit, so
            // we can set the C flag
            if (Bits.getUnsignedByte(sum) < Bits.getUnsignedByte(b)) {
                flag |= C;
            } else {
                flag &= C_CLEAR;
            }
        }

        return flag;
    }

    /**
     * @param flag The original value of the F register
     * @param b The value of the register you are decrementing
     * @return The new value of the F register
     */
    public static byte decFlags(byte flag, byte b) {
        return addFlags(flag, b, (byte) 1, false);
    }

    public static byte subFlags(byte flag, byte b, byte addend) {
        return addFlags(flag, b, addend, true);
    }

    /**
     * @param flag The original value of the F register
     * @param b The value of the register you are subtracting from
     * @param subtrahend The value you are subtracting from the register
     * @param adjustCarry Whether or not this should influence the carry flag
     * @return The new value of the F register
     */
    public static byte subFlags(byte flag, byte b, byte subtrahend, boolean adjustCarry) {
        byte diff = (byte) (b - subtrahend);

        // If the diff is zero, then set the Z flag, otherwise clear it
        if (diff == 0) {
            flag |= Z;
        } else {
            flag &= Z_CLEAR;
        }

        // This is a subtraction, so we want to set the N flag
        flag |= N;

        // If the diff's right nibble is greater than the original right nibble, than we must have
        // underflowed the nibble, so we can set the H flag. Note that because we are only checking
        // the right nibble, we don't have to worry about the sign bit, and therefore do not have to
        // convert to an unsigned byte.
        byte originalRightNibble = Bits.getLowNibble(b);
        byte newRightNibble = Bits.getLowNibble(diff);
        if (newRightNibble > originalRightNibble) {
            flag |= H;
        } else {
            flag &= H_CLEAR;
        }

        if (adjustCarry) {
            // If the diff is greater than the original value, we must have underflowed the byte,
            // so we can set the C flag
            if (Bits.getUnsignedByte(diff) > Bits.getUnsignedByte(b)) {
                flag |= C;
            } else {
                flag &= C_CLEAR;
            }
        }

        return flag;
    }

    /**
     * @return The value of the Z flag
     */
    public static byte getZ(byte flags) {
        return (byte) ((flags & Z) >> Z_POSITION);
    }

    /**
     * Set the value of the Z flag.
     * @param flags The current value of the flags
     * @param value The value to se the flag to
     * @return The new value of the flags
     */
    public static byte setZ(byte flags, int value) {
        if (value == 0) {
            return (byte) (flags & Z_CLEAR);
        }
        return (byte) (flags | Z);
    }

    /**
     * @return The value of the N flag
     */
    public static byte getN(byte flags) {
        return (byte) ((flags & N) >> N_POSITION);
    }

    /**
     * Set the value of the N flag.
     * @param flags The current value of the flags
     * @param value The value to se the flag to
     * @return The new value of the flags
     */
    public static byte setN(byte flags, int value) {
        if (value == 0) {
            return (byte) (flags & N_CLEAR);
        }
        return (byte) (flags | N);
    }

    /**
     * @return The value of the H flag
     */
    public static byte getH(byte flags) {
        return (byte) ((flags & H) >> H_POSITION);
    }

    /**
     * Set the value of the H flag.
     * @param flags The current value of the flags
     * @param value The value to se the flag to
     * @return The new value of the flags
     */
    public static byte setH(byte flags, int value) {
        if (value == 0) {
            return (byte) (flags & H_CLEAR);
        }
        return (byte) (flags | H);
    }

    /**
     * @return The value of the C flag
     */
    public static byte getC(byte flags) {
        return (byte) ((flags & C) >> C_POSITION);
    }

    /**
     * Set the value of the C flag.
     * @param flags The current value of the flags
     * @param value The value to se the flag to
     * @return The new value of the flags
     */
    public static byte setC(byte flags, int value) {
        if (value == 0) {
            return (byte) (flags & C_CLEAR);
        }
        return (byte) (flags | C);
    }
}
