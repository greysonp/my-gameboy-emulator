package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Decrements the contents of memory specified by register pair BC by 1.
 *
 * Opcode: 0x0B
 */
public class DEC_BC extends DEC_RR {

    @Override
    protected int getHighByteRegister() {
        return Cpu.State.B;
    }

    @Override
    protected int getLowByteRegister() {
        return Cpu.State.C;
    }
}
