package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Loads an 8-bit value directly into register A.
 *
 * Opcode: 0x3E
 */
public class LD_A_N extends LD_R_N {

    protected int getRegister() {
        return Cpu.State.A;
    }
}
