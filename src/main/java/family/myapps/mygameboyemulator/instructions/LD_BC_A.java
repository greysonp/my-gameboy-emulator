package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Writes the contents of register A to the location in memory specified by the register pair BC.
 *
 * Opcode: 0x02
 */
public class LD_BC_A extends LD_RR_A {

    @Override
    protected int getHighByteRegister() {
        return Cpu.State.B;
    }

    @Override
    protected int getLowByteRegister() {
        return Cpu.State.C;
    }
}
