package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Places a 16-bit word into the register pair BC.
 * B holds high byte, and C holds the low byte.
 *
 * Opcode: 0x01
 */
public class LD_BC_NN extends LD_RR_NN {

    @Override
    protected int getHighByteRegister() {
        return Cpu.State.B;
    }

    @Override
    protected int getLowByteRegister() {
        return Cpu.State.C;
    }
}
