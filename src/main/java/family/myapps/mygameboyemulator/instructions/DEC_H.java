package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Decrements the contents of register H.
 *
 * Opcode: 0x25
 */
public class DEC_H extends DEC_R {

    @Override
    protected int getRegister() {
        return Cpu.State.H;
    }
}
