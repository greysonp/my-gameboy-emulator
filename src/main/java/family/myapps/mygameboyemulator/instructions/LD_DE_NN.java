package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Places a 16-bit word into the register pair DE.
 * D holds high byte, and E holds the low byte.
 *
 * Opcode: 0x11
 */
public class LD_DE_NN extends LD_RR_NN {
    @Override
    protected int getHighByteRegister() {
        return Cpu.State.D;
    }

    @Override
    protected int getLowByteRegister() {
        return Cpu.State.E;
    }
}
