package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Increments the contents of register L by 1.
 *
 * Opcode: 0x2C
 */
public class INC_L extends INC_R {

    @Override
    protected int getRegister() {
        return Cpu.State.L;
    }
}
