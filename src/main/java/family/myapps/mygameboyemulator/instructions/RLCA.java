package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

import static family.myapps.mygameboyemulator.Cpu.State.A;

/**
 * Rotates the value of register A to the left.
 * The leftmost bit is placed both at bit 0 of A, as well as the carry flag.
 *
 * Opcode: 0x07
 */
public class RLCA implements Instruction {

    @Override
    public void execute(Cpu.State cpu, Memory memory) {
        // Get the leftmost bit of A before we shift, so we can put it where it needs to go
        byte leftmostBit = (byte) (cpu.register[A] >> 7);

        // Shift the register and copy the leftmost bit into bit 0
        byte newVal = (byte) (cpu.register[A] << 1);
        newVal |= leftmostBit;
        cpu.register[A] = newVal;

        // The flags should all be cleared, except we set the carry flag equal to the leftmost bit
        cpu.flag = Flags.setC((byte) 0, leftmostBit);
    }

    @Override
    public int getCost() {
        return 4;
    }
}
