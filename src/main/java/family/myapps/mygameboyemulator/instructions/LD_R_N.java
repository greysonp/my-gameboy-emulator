package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

/**
 * Loads an 8-bit value directly into the register.
 */
public abstract class LD_R_N implements Instruction {

    @Override
    public void execute(Cpu.State cpu, Memory memory) {
        cpu.register[getRegister()] = cpu.program[cpu.pc];
        cpu.pc++;
    }

    /**
     * @return The index of the register you want to increment.
     */
    protected abstract int getRegister();

    @Override
    public int getCost() {
        return 8;
    }
}
