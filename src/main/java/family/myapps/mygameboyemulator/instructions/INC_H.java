package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Increments the contents of register H by 1.
 *
 * Opcode: 0x24
 */
public class INC_H extends INC_R {

    @Override
    protected int getRegister() {
        return Cpu.State.H;
    }
}
