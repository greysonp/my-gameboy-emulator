package family.myapps.mygameboyemulator;

import static family.myapps.mygameboyemulator.Cpu.State.H;
import static family.myapps.mygameboyemulator.Cpu.State.L;

public final class Registers {

    public static short getHL(Cpu.State cpu) {
       return Bits.joinBytes(cpu.register[H], cpu.register[L]);
    }

    public static void setHL(Cpu.State cpu, short value) {
        cpu.register[H] = Bits.getHighByte(value);
        cpu.register[L] = Bits.getLowByte(value);
    }
}
