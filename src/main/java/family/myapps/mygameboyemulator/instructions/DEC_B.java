package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

/**
 * Decrements the contents of register B.
 *
 * Opcode: 0x05
 */
public class DEC_B extends DEC_R {

    @Override
    protected int getRegister() {
        return Cpu.State.B;
    }
}
