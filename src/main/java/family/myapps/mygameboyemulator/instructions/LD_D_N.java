package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Loads an 8-bit value directly into register D.
 *
 * Opcode: 0x16
 */
public class LD_D_N extends LD_R_N {

    protected int getRegister() {
        return Cpu.State.D;
    }
}
