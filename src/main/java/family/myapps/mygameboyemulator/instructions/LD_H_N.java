package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Loads an 8-bit value directly into register H.
 *
 * Opcode: 0x26
 */
public class LD_H_N extends LD_R_N {

    protected int getRegister() {
        return Cpu.State.H;
    }
}
