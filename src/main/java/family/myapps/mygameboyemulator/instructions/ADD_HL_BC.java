package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Stores the sum of HL and BC in HL.
 *
 * Opcode: 0x09
 */
public class ADD_HL_BC extends ADD_HL_RR {

    @Override
    protected short getRegisterPairValue(Cpu.State cpu) {
        return cpu.getBC();
    }
}
