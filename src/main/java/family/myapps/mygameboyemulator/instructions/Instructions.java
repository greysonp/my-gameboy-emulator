package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Bits;

public class Instructions {

    private static Instruction[] instructions;
    static {
        instructions[0x00] = new NOP();
        instructions[0x01] = new LD_BC_NN();
        instructions[0x02] = new LD_BC_A();
        instructions[0x03] = new INC_BC();
        instructions[0x04] = new INC_B();
        instructions[0x05] = new DEC_B();
        instructions[0x06] = new LD_B_N();
        instructions[0x07] = new RLCA();
        instructions[0x08] = new LD_NN_SP();
        instructions[0x09] = new ADD_HL_BC();
        instructions[0x0A] = new LD_A_BC();
        instructions[0x0B] = new DEC_BC();
        instructions[0x0C] = new INC_C();
        instructions[0x0D] = new DEC_C();
        instructions[0x0E] = new LD_C_N();
        instructions[0x11] = new LD_DE_NN();
        instructions[0x12] = new LD_DE_A();
        instructions[0x13] = new INC_DE();
        instructions[0x14] = new INC_D();
        instructions[0x15] = new DEC_D();
        instructions[0x16] = new LD_D_N();
        instructions[0x17] = new RLA();
        instructions[0x19] = new ADD_HL_DE();
        instructions[0x1A] = new LD_A_DE();
        instructions[0x1B] = new DEC_DE();
        instructions[0x1C] = new INC_E();
        instructions[0x1D] = new DEC_E();
        instructions[0x1E] = new LD_E_N();
        instructions[0x21] = new LD_HL_NN();
        instructions[0x22] = new LD_HLI_A();
        instructions[0x23] = new INC_HL();
        instructions[0x24] = new INC_H();
        instructions[0x25] = new DEC_H();
        instructions[0x26] = new LD_H_N();
        instructions[0x29] = new ADD_HL_HL();
        instructions[0x2A] = new LD_A_HLI();
        instructions[0x2B] = new DEC_HL();
        instructions[0x2C] = new INC_L();
        instructions[0x2D] = new DEC_L();
        instructions[0x2E] = new LD_L_N();
        instructions[0x31] = new LD_SP_NN();
        instructions[0x32] = new LD_HLD_A();
        instructions[0x33] = new INC_SP();
        instructions[0x39] = new ADD_HL_SP();
        instructions[0x3A] = new LD_A_HLD();
        instructions[0x3B] = new DEC_SP();
        instructions[0x3C] = new INC_A();
        instructions[0x3D] = new DEC_A();
        instructions[0x3E] = new LD_A_N();
    }

    public Instruction get(byte code) {
        // TODO: Handle the CB extended codes
        return instructions[Bits.getUnsignedByte(code)];
    }

    // 1001 + 0001 = 1010
    // 1001 + 0001 = 0
}
