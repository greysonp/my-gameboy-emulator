package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Loads an 8-bit value directly into register C.
 *
 * Opcode: 0x0E
 */
public class LD_C_N extends LD_R_N {

    protected int getRegister() {
        return Cpu.State.C;
    }
}
