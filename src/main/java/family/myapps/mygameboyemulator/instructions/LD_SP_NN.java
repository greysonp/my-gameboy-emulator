package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

/**
 * Places a 16-bit word into the stack pointer.
 * Because the Z80 is little endian, the low byte is provided first.
 *
 * Opcode: 0x31
 */
public class LD_SP_NN implements Instruction {

    @Override
    public void execute(Cpu.State cpu, Memory memory) {
        byte lowByte = cpu.program[cpu.pc];
        cpu.pc++;
        byte highByte = cpu.program[cpu.pc];
        cpu.pc++;

        cpu.sp = 0;
        cpu.sp |= highByte << 8;
        cpu.sp |= lowByte;
    }

    @Override
    public int getCost() {
        return 12;
    }
}
