package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

/**
 * No-op. Does nothing.
 *
 * Opcode: 0x00
 */
public class NOP implements Instruction {

    @Override
    public void execute(Cpu.State cpu, Memory memory) {
        // Do nothing
    }

    @Override
    public int getCost() {
        return 4;
    }
}
