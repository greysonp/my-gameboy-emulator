package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Bits;
import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

import static family.myapps.mygameboyemulator.Cpu.State.H;
import static family.myapps.mygameboyemulator.Cpu.State.L;

/**
 * Stores the sum of HL and a register pair in HL.
 *
 * The carry flag is set if you overflow the 15th bit, and the half carry flag is set if you
 * overflow the 11th bit.
 */
abstract class ADD_HL_RR implements Instruction {
    @Override
    public void execute(Cpu.State cpu, Memory memory) {
        // Sum the register pair with HL and store the result in HL
        short hl = Bits.joinBytes(cpu.register[H], cpu.register[L]);
        short rr = getRegisterPairValue(cpu);

        short sum = (short) (hl + rr);
        byte originalH = cpu.register[H];
        cpu.register[H] = Bits.getHighByte(sum);
        cpu.register[L] = Bits.getLowByte(sum);

        // This is not a subtraction, so we clear the N flag
        cpu.flag = Flags.setN(cpu.flag, 0);

        // If the new low nibble of H is less than the old low nibble of H, then we must have
        // overflowed the nibble, meaning we can set the half carry flag
        byte originalLowNibble = Bits.getLowNibble(originalH);
        byte newLowNibble = Bits.getLowNibble(cpu.register[H]);
        if (newLowNibble < originalLowNibble) {
            cpu.flag = Flags.setH(cpu.flag, 1);
        } else {
            cpu.flag = Flags.setH(cpu.flag, 0);
        }

        // If the new sum is less than the original value of HL, then we must have overflowed the
        // byte, so we can set the carry flag
        if (Bits.getUnsignedShort(sum) < Bits.getUnsignedShort(hl)) {
            cpu.flag = Flags.setC(cpu.flag, 1);
        } else {
            cpu.flag = Flags.setC(cpu.flag, 0);
        }
    }

    /**
     * @return The value of the register pair
     */
    protected abstract short getRegisterPairValue(Cpu.State cpu);

    @Override
    public int getCost() {
        return 8;
    }
}
