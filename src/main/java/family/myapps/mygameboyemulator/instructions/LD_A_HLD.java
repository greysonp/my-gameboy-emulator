package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;
import family.myapps.mygameboyemulator.Registers;

import static family.myapps.mygameboyemulator.Cpu.State.H;
import static family.myapps.mygameboyemulator.Cpu.State.L;

/**
 * Loads the value in memory pointed to by register pair HL into register A, then decrements the
 * value of register pair HL.
 *
 * Opcode: Ox3A
 */
public class LD_A_HLD extends LD_A_RR {

    @Override
    public void execute(Cpu.State cpu, Memory memory) {
        super.execute(cpu, memory);
        Registers.setHL(cpu, (short) (Registers.getHL(cpu) - 1));
    }

    @Override
    protected int getHighByteRegister() {
        return H;
    }

    @Override
    protected int getLowByteRegister() {
        return L;
    }
}
