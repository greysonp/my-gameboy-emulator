package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

/**
 * Decrements the contents of a register by 1.
 */
abstract class DEC_R implements Instruction {

    @Override
    public void execute(Cpu.State cpu, Memory memory) {
        int r = getRegister();
        cpu.flag = Flags.decFlags(cpu.flag, cpu.register[r]);
        cpu.register[r]--;
    }

    /**
     * @return The index of the register you want to increment
     */
    protected abstract int getRegister();

    @Override
    public int getCost() {
        return 4;
    }
}
