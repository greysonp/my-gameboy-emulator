package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Loads an 8-bit value directly into register E.
 *
 * Opcode: 0x1E
 */
public class LD_E_N extends LD_R_N {

    protected int getRegister() {
        return Cpu.State.E;
    }
}
