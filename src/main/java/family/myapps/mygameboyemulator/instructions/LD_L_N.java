package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Loads an 8-bit value directly into register L.
 *
 * Opcode: 0x2E
 */
public class LD_L_N extends LD_R_N {

    protected int getRegister() {
        return Cpu.State.L;
    }
}
