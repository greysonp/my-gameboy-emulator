package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Increments the contents of register C by 1.
 *
 * Opcode: 0x0C
 */
public class INC_C extends INC_R {

    @Override
    protected int getRegister() {
        return Cpu.State.C;
    }
}

