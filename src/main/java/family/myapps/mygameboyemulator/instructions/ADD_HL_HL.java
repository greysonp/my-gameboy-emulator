package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Stores the sum of HL and HL in HL.
 *
 * Opcode: 0x29
 */
public class ADD_HL_HL extends ADD_HL_RR {

    @Override
    protected short getRegisterPairValue(Cpu.State cpu) {
        return cpu.getHL();
    }
}
