package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Stores the sum of HL and DE in HL.
 *
 * Opcode: 0x19
 */
public class ADD_HL_DE extends ADD_HL_RR {

    @Override
    protected short getRegisterPairValue(Cpu.State cpu) {
        return cpu.getDE();
    }
}
