package family.myapps.mygameboyemulator;

/**
 * A helper class to deal with various bit operations.
 */
public final class Bits {

    /**
     * Concatenates two bytes into a single short. Useful for things like joining two registers
     * to form a 16-bit address.
     * @param b1 High byte
     * @param b2 Low byte
     * @return A 16-bit short in the form of HighByteLowByte
     */
    public static short joinBytes(byte b1, byte b2) {
        return (short) (b1 << 8 | (b2 & 0xFF));
    }

    /**
     * @return The higher byte in the short.
     */
    public static byte getHighByte(short s) {
        short isolatedByte = (short) (s & 0xFF00);
        return (byte) (isolatedByte >> 8);
    }

    /**
     * @return The lower byte in the short.
     */
    public static byte getLowByte(short s) {
        return (byte) (s & 0x00FF);
    }

    /**
     * @return The high nibble (4 bits)
     */
    public static byte getHighNibble(byte b) {
        return (byte) ((b >> 4) & 0x0F);
    }

    /**
     * @return The low nibble (4 bits)
     */
    public static byte getLowNibble(byte b) {
        return (byte) (b & 0x0F);
    }
    /**
     * @return An integer representing the unsigned value of the given byte
     */
    public static int getUnsignedByte(byte b) {
        // In Java, all primitives (except char) are signed. Here, we get the signed value by
        // converting it to an int (the result of a bitwise operation is an int).
        return b & 0xFF;
    }

    /**
     * @return An integer representing the unsigned value of the given short
     */
    public static int getUnsignedShort(short s) {
        // In Java, all primitives (except char) are signed. Here, we get the signed value by
        // converting it to an int (the result of a bitwise operation is an int).
        return s & 0xFFFF;
    }
}
