package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Increments the contents of register E by 1.
 *
 * Opcode: 0x1C
 */
public class INC_E extends INC_R {

    @Override
    protected int getRegister() {
        return Cpu.State.E;
    }
}
