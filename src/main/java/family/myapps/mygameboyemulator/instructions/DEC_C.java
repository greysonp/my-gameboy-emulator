package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Decrements the contents of register C.
 *
 * Opcode: 0x0D
 */
public class DEC_C extends DEC_R {

    @Override
    protected int getRegister() {
        return Cpu.State.C;
    }
}
