package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Decrements the contents of memory specified by register pair DE by 1.
 *
 * Opcode: 0x1B
 */
public class DEC_DE extends DEC_RR {

    @Override
    protected int getHighByteRegister() {
        return Cpu.State.D;
    }

    @Override
    protected int getLowByteRegister() {
        return Cpu.State.E;
    }
}
