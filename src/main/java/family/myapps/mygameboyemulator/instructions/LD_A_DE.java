package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Loads the value in memory pointed to by register pair DE into register A.
 *
 * Opcode: Ox1A
 */
public class LD_A_DE extends LD_A_RR {

    @Override
    protected int getHighByteRegister() {
        return Cpu.State.D;
    }

    @Override
    protected int getLowByteRegister() {
        return Cpu.State.E;
    }
}
