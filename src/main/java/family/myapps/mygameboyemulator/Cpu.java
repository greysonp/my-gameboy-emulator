package family.myapps.mygameboyemulator;

public class Cpu {

    public Cpu() {

    }

    public static final class State {
        public static final int A = 0;
        public static final int B = 1;
        public static final int C = 2;
        public static final int D = 3;
        public static final int E = 4;
        public static final int H = 5;
        public static final int L = 6;

        public byte[] register = new byte[7];
        public byte flag;
        public short pc;
        public short sp;

        public final byte[] program;

        public State(byte[] program) {
            this.program = program;
        }

        public short getBC() {
            return getRR(B, C);
        }

        public void setBC(short value) {
            setRR(B, C, value);
        }

        public short getDE() {
            return getRR(D, E);
        }

        public void setDE(short value) {
            setRR(D, E, value);
        }

        public short getHL() {
            return getRR(H, L);
        }

        public void setHL(short value) {
            setRR(H, L, value);
        }

        private short getRR(int r1, int r2) {
            return Bits.joinBytes(register[r1], register[r2]);
        }

        private void setRR(int r1, int r2, short value) {
            register[r1] = Bits.getHighByte(value);
            register[r2] = Bits.getLowByte(value);
        }
    }
}
