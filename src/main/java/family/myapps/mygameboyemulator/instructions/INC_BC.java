package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Increments the contents of memory specified by the register pair BC by 1.
 *
 * Opcode: 0x03
 */
public class INC_BC extends INC_RR {

    @Override
    protected int getHighByteRegister() {
        return Cpu.State.B;
    }

    @Override
    protected int getLowByteRegister() {
        return Cpu.State.C;
    }
}
