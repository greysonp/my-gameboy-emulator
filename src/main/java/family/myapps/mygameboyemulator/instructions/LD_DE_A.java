package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Writes the contents of register A to the location in memory specified by the register pair DE.
 *
 * Opcode: 0x12
 */
public class LD_DE_A extends LD_RR_A {

    @Override
    protected int getHighByteRegister() {
        return Cpu.State.D;
    }

    @Override
    protected int getLowByteRegister() {
        return Cpu.State.E;
    }
}
