package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

/**
 * Loads an 8-bit value directly into register B.
 *
 * Opcode: 0x06
 */
public class LD_B_N extends LD_R_N {

    protected int getRegister() {
        return Cpu.State.B;
    }
}
