package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Loads the value in memory pointed to by register pair BC into register A.
 *
 * Opcode: Ox0A
 */
public class LD_A_BC extends LD_A_RR {

    @Override
    protected int getHighByteRegister() {
        return Cpu.State.B;
    }

    @Override
    protected int getLowByteRegister() {
        return Cpu.State.C;
    }
}
