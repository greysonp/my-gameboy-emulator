package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Increments the contents of memory specified by the register pair HL by 1.
 *
 * Opcode: 0x23
 */
public class INC_HL extends INC_RR {

    @Override
    protected int getHighByteRegister() {
        return Cpu.State.H;
    }

    @Override
    protected int getLowByteRegister() {
        return Cpu.State.L;
    }
}
