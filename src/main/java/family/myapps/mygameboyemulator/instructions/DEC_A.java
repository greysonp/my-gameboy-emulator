package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Decrements the contents of register A.
 *
 * Opcode: 0x3D
 */
public class DEC_A extends DEC_R {

    @Override
    protected int getRegister() {
        return Cpu.State.A;
    }
}
