package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

/**
 * Decrements the contents of memory specified by SP by 1.
 *
 * Opcode: 0x3B
 */
public class DEC_SP implements Instruction {

    @Override
    public void execute(Cpu.State cpu, Memory memory) {
        short address = cpu.sp;
        byte currentValue = memory.readByte(address);
        byte newValue = (byte) (currentValue - 1);
        memory.writeByte(address, newValue);
    }

    @Override
    public int getCost() {
        return 8;
    }
}
