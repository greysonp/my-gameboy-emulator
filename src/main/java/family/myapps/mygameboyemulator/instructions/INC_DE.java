package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Increments the contents of memory specified by the register pair DE by 1.
 *
 * Opcode: 0x13
 */
public class INC_DE extends INC_RR {

    @Override
    protected int getHighByteRegister() {
        return Cpu.State.D;
    }

    @Override
    protected int getLowByteRegister() {
        return Cpu.State.E;
    }
}
