package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

/**
 * Places a 16-bit word into a register pair.
 * The first register holds high byte, and second register holds the low byte.
 * Because the Z80 is little endian, the low byte is provided first.
 */
abstract class LD_RR_NN implements Instruction {

    @Override
    public void execute(Cpu.State cpu, Memory memory) {
        cpu.register[getLowByteRegister()] = cpu.program[cpu.pc];
        cpu.pc++;
        cpu.register[getHighByteRegister()] = cpu.program[cpu.pc];
        cpu.pc++;
    }

    /**
     * @return The index of the register that holds the higher byte.
     */
    protected abstract int getHighByteRegister();

    /**
     * @return The index of the register that holds the lower byte.
     */
    protected abstract int getLowByteRegister();

    @Override
    public int getCost() {
        return 12;
    }
}
