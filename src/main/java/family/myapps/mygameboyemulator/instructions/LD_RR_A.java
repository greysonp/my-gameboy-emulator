package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Bits;
import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

/**
 * Writes the contents of register A to the location in memory specified by a register pair.
 */
abstract class LD_RR_A implements Instruction {

    @Override
    public void execute(Cpu.State cpu, Memory memory) {
        short address = Bits.joinBytes(
                cpu.register[getHighByteRegister()],
                cpu.register[getLowByteRegister()]);
        memory.writeByte(address, cpu.register[Cpu.State.A]);
    }

    /**
     * @return The index of the register that holds the higher byte.
     */
    protected abstract int getHighByteRegister();

    /**
     * @return The index of the register that holds the lower byte.
     */
    protected abstract int getLowByteRegister();

    @Override
    public int getCost() {
        return 8;
    }
}
