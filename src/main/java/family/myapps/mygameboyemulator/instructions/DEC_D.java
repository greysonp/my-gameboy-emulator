package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Decrements the contents of register D.
 *
 * Opcode: 0x15
 */
public class DEC_D extends DEC_R {

    @Override
    protected int getRegister() {
        return Cpu.State.D;
    }
}
