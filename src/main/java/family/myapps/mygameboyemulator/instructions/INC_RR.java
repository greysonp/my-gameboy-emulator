package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Bits;
import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

/**
 * Increments the contents of memory specified by a register pair by 1.
 */
abstract class INC_RR implements Instruction {

    @Override
    public void execute(Cpu.State cpu, Memory memory) {
        short address = Bits.joinBytes(
                cpu.register[getHighByteRegister()],
                cpu.register[getLowByteRegister()]);
        byte currentValue = memory.readByte(address);
        byte newValue = (byte) (currentValue + 1);
        memory.writeByte(address, newValue);
    }

    /**
     * @return The index of the register that holds the higher byte of the address.
     */
    protected abstract int getHighByteRegister();

    /**
     * @return The index of the register that holds the lower byte of the address.
     */
    protected abstract int getLowByteRegister();

    @Override
    public int getCost() {
        return 8;
    }
}
