package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Increments the contents of register B by 1.
 *
 * Opcode: 0x04
 */
public class INC_B extends INC_R {

    @Override
    protected int getRegister() {
        return Cpu.State.B;
    }
}
