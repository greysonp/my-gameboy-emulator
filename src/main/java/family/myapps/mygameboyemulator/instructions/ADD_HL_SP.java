package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Cpu;

/**
 * Stores the sum of HL and SP in HL.
 *
 * Opcode: 0x39
 */
public class ADD_HL_SP extends ADD_HL_RR {

    @Override
    protected short getRegisterPairValue(Cpu.State cpu) {
        return cpu.sp;
    }
}
