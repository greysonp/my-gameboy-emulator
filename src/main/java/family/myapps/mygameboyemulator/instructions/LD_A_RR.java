package family.myapps.mygameboyemulator.instructions;

import family.myapps.mygameboyemulator.Bits;
import family.myapps.mygameboyemulator.Cpu;
import family.myapps.mygameboyemulator.Memory;

/**
 * Loads the value in memory pointed to by the register pair into register A.
 */
abstract class LD_A_RR implements Instruction {

    @Override
    public void execute(Cpu.State cpu, Memory memory) {
        short address = Bits.joinBytes(
                cpu.register[getHighByteRegister()],
                cpu.register[getLowByteRegister()]);
        cpu.register[Cpu.State.A] = memory.readByte(address);
    }

    /**
     * @return The index of the register that holds the higher byte.
     */
    protected abstract int getHighByteRegister();

    /**
     * @return The index of the register that holds the lower byte.
     */
    protected abstract int getLowByteRegister();

    @Override
    public int getCost() {
        return 8;
    }
}
